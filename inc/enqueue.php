<?php 
function add_css_js(){
	//wp_enqueue_style('name',get_template_directory_uri().'//',array(),'1.0.0','all');
	
	wp_enqueue_style('googleapis','//fonts.googleapis.com/css?family=Raleway:400,700',array(),'1.0.0','all');
	wp_enqueue_style('awesome',get_template_directory_uri().'/css/font-awesome/css/font-awesome.min.css',array(),'1.0.0','all');
	wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap/bootstrap.min.css',array(),'1.0.0','all');
	wp_enqueue_style('superslides',get_template_directory_uri().'/css/superslide/superslides.css',array(),'1.0.0','all');
	wp_enqueue_style('fancybox',get_template_directory_uri().'/css/fancybox/jquery.fancybox.css',array(),'1.0.0','all');
	wp_enqueue_style('lightbox',get_template_directory_uri().'/css/nivo-lightbox/nivo-lightbox.css',array(),'1.0.0','all');
	wp_enqueue_style('default',get_template_directory_uri().'/css/themes/default/default.css',array(),'1.0.0','all');
	wp_enqueue_style('style',get_template_directory_uri().'/css/style.css',array(),'1.0.0','all');
	wp_enqueue_style('responsive',get_template_directory_uri().'/css/responsive.css',array(),'1.0.0','all');
	wp_enqueue_style('animate',get_template_directory_uri().'/css/animate/animate.css',array(),'1.0.0','all');
	wp_enqueue_style('carousel',get_template_directory_uri().'/css/owl-carousel/owl.carousel.css',array(),'1.0.0','all');
	wp_enqueue_style('theme',get_template_directory_uri().'/css/owl-carousel/owl.theme.css',array(),'1.0.0','all');
	wp_enqueue_style('carouselowl',get_template_directory_uri().'/css/owl-carousel/owl.transitions.css',array(),'1.0.0','all');
	
	//add_js
	
	//wp_enqueue_script('name',get_template_directory_uri().'//',array(),'1.0.0',true);
	
	wp_enqueue_script('script',get_template_directory_uri().'/js/script.js',array('jquery'),'1.0.0',true);
	wp_enqueue_script('bootstrapjs',get_template_directory_uri().'/js/bootstrap/bootstrap.min.js',array('jquery'),'1.0.0',true);
	wp_enqueue_script('fancyboxjs',get_template_directory_uri().'/js/fancybox/jquery.fancybox.pack.js',array('jquery'),'1.0.0',true);
	wp_enqueue_script('lightboxjs',get_template_directory_uri().'/js/nivo-lightbox/nivo-lightbox.min.js',array('jquery'),'1.0.0',true);
	wp_enqueue_script('owl.carousel',get_template_directory_uri().'/js/owl-carousel/owl.carousel.min.js',array('jquery'),'1.0.0',true);
	wp_enqueue_script('owl.carousel','//maps.googleapis.com/maps/api/js?key=AIzaSyBL6gbhsnCEt4FS9D6BBl3mZO1xy-NcwpE&sensor=false',array('jquery'),'1.0.0',true);
	wp_enqueue_script('owl.easing',get_template_directory_uri().'/js/jquery-easing/jquery.easing.1.3.js',array('jquery'),'1.0.0',true);
	wp_enqueue_script('owl.superslides',get_template_directory_uri().'/js/superslide/jquery.superslides.js',array('jquery'),'1.0.0',true);
	wp_enqueue_script('owl.wow',get_template_directory_uri().'/js/wow/wow.min.js',array('jquery'),'1.0.0',true);
	
} add_action('wp_enqueue_scripts','add_css_js');









?>