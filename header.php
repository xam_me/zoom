<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Zoom UI Kit One Page Portfolios Template</title>

     


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
		<?php wp_head();?>
    </head>
    <body <?php body_class();?>>
		<!-- <div class='preloader'><div class='loaded'>&nbsp;</div></div> -->
        <header id="home" class="header navbar-fixed-top">
            <div class="navbar navbar-default main-menu">

                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a  href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand"><img src="<?php echo esc_url(wp_get_attachment_image_src(cs_get_option('head_logo'),'full')[0]);?>" alt="Logo" /></a>
                        <!-- <a class="navbar-brand" href="index.html"></a> -->
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#works" class=""><?php echo esc_html(cs_get_option('menu_1'));?></a></li>
                            <li><a href="#description_second" class=""><?php echo esc_html(cs_get_option('menu_2'));?></a></li>
                            <li><a href="#pricing" class=""><?php echo esc_html(cs_get_option('menu_3'));?></a></li>
                            <li><a href="#downloadApps" class=""><?php echo esc_html(cs_get_option('menu_4'));?></a></li>
                        </ul>
					
						
						

                    </div>
                </div>
            </div> <!-- end of navbar -->
        </header>